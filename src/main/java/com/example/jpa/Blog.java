package com.example.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.context.request.RequestContextListener;

@SpringBootApplication
@EnableJpaAuditing
public class Blog {
	public static void main(String[] args) {
		SpringApplication.run(Blog.class, args);
	}

}
