package com.example.jpa;

import com.example.jpa.security.AuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@ComponentScan(basePackages = { "com.example.jpa.security" })
@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private LogoutSuccessHandler myLogoutSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    public SecurityConfiguration() {
        super();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/profile").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/userFeed").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/drafts").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/search-post").permitAll()
                .antMatchers("/sign-in").permitAll()
                .antMatchers("/sign-up").permitAll()
                .antMatchers("/new-post").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/savePost").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/post/*").permitAll()
                .antMatchers("/drafts/*").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/filter-post/*").permitAll()
                .antMatchers("/post/*/edit-post").hasAuthority("READ_PRIVILEGE")
                .antMatchers("post/*/update-post").hasAuthority("READ_PRIVILEGE")
                .antMatchers("post/*/delete-post").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/post/my-article/*").hasAuthority("READ_PRIVILEGE")

                .antMatchers("/sign-up").permitAll()
                .antMatchers("/forgetPassword*").permitAll()
                .antMatchers("/badUser*").permitAll()
                .antMatchers("/registrationConfirm*").permitAll()
                .antMatchers("/resendVerificationMail").permitAll()
                .antMatchers("/author/resetPassword").permitAll()
                .antMatchers("/author/changePassword").permitAll()
                .antMatchers("/author/savePassword").permitAll()

                .antMatchers("/post/*/save-comment").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/post/*/comment/*/delete-comment").hasAuthority("READ_PRIVILEGE")
                .antMatchers("/post/*/comment/*").hasAuthority("READ_PRIVILEGE")
                .antMatchers("post/*/comment/*/update-comment").hasAuthority("READ_PRIVILEGE")

                .antMatchers("/setting").hasAuthority("READ_PRIVILEGE")

                .antMatchers("/users").hasAuthority("WRITE_PRIVILEGE")

                .anyRequest().hasAuthority("READ_PRIVILEGE")
                .and()
                .formLogin()
                .loginPage("/sign-in")
                .failureUrl("/sign-in?error=true")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .permitAll()
                .and()
                .sessionManagement()
                .invalidSessionUrl("/invalidSession.html")
                .maximumSessions(1).sessionRegistry(sessionRegistry()).and()
                .sessionFixation().none()
                .and()
                .logout()
                .logoutSuccessHandler(myLogoutSuccessHandler)
                .logoutRequestMatcher(new AntPathRequestMatcher("/sign-out"))
                .invalidateHttpSession(false)
                .logoutSuccessUrl("/sign-in.html")
                .deleteCookies("JSESSIONID")
                .permitAll()
                .and()
                .headers().frameOptions().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**","/webjars/**");
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        final AuthenticationProvider authProvider = new AuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

}