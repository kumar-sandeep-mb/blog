package com.example.jpa.dto;

import com.example.jpa.validation.PasswordMatches;
import com.example.jpa.validation.ValidEmail;
import com.example.jpa.validation.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@PasswordMatches
public class AuthorDto {
    @NotNull
    @Size(min = 1, max=30)
    private String name;

    @ValidEmail
    private String email;

    @ValidPassword
    private String password;

    @NotNull
    private String matchingPassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(final String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("AuthorDto [name=").append(name).append(", password=").append(password)
                .append(", matchingPassword=").append(matchingPassword).append(", email=").append(email);
        return builder.toString();
    }

}
