package com.example.jpa.service;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.model.Author;
import com.example.jpa.model.Post;
import com.example.jpa.model.Tag;
import com.example.jpa.repository.PostRepository;
import com.example.jpa.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class PostService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    PostRepository postRepository;
    @Autowired
    TagRepository tagRepository;

    public Page<Post> getAllPost(int page) {
        return postRepository.findAllPublishedPost(PageRequest.of(page, 4, Sort.by("updatedAt").descending()));
    }

    public Page<Post> getAuthorPost(int page, Author author) {
        return postRepository.findByAuthor(author, PageRequest.of(page, 4, Sort.by("updatedAt").descending()));
    }

    public Page<Post> getAuthorDraft(int page, Author author) {
        return postRepository.findAllDraftOfAuthor(author, PageRequest.of(page, 4, Sort.by("updatedAt").descending()));
    }

    public Page<Post> getBySearch(int page, String searchString) {
        return postRepository.searchPost(searchString, PageRequest.of(page, 4, Sort.by("updatedAt").descending()));
    }

    public ModelAndView showNewPostPage() {
        LOGGER.debug("Rendering registration page.");
        final Post post = new Post();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("post", post);
        modelAndView.setViewName("new-post");
        return modelAndView;
    }

    public ModelAndView save(ModelMap modelMap, Post post, Authentication authentication, String isPublished, String tagString) {
        LOGGER.debug("rendering savePost");
        ModelAndView modelAndView = new ModelAndView();
        if (post.getTitle().trim().isEmpty()) {
            LOGGER.error("Post title empty");
            modelAndView.addObject("successMessage", "empty title");
            return modelAndView;
        }
        if (post.getDescription().trim().isEmpty()) {
            LOGGER.error("Post description empty");
            modelAndView.addObject("successMessage", "empty description");
            return modelAndView;
        }
        if (post.getContent().trim().isEmpty()) {
            LOGGER.error("Post content empty");
            modelAndView.addObject("successMessage", "empty content");
            return modelAndView;
        }
        if (postRepository.findByTitle(post.getTitle().trim()) != null) {
            LOGGER.error("Post title already exist");
            modelAndView.addObject("successMessage", "title already exist");
            return modelAndView;
        }
        if (isPublished.equals("publish")) {
            post.setPublished(true);
        } else {
            post.setPublished(false);
        }
        post.setAuthor((Author) authentication.getPrincipal());
        saveTag(tagString, post);
        LOGGER.debug("new post saved with details: {}", post);
        modelAndView.addObject("successMessage", "post saved successfully");
        return modelAndView;
    }

    public ModelAndView showUpdatePostPage(Long postId) {
        LOGGER.debug("Rendering registration page.");
        final Post post = postRepository.findById(postId).get();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("post", post);
        modelAndView.setViewName("update-post");
        return modelAndView;
    }

    public ModelAndView update(Long postId, Post updatedPost, String isPublished, String tagString) {
        LOGGER.debug("rendering updatePost");
        ModelAndView modelAndView = new ModelAndView();
        if (updatedPost.getTitle().trim().isEmpty()) {
            LOGGER.error("Post title empty");
            modelAndView.addObject("successMessage", "empty title");
            modelAndView.setViewName("update-post");
            return modelAndView;
        }
        if (updatedPost.getDescription().trim().isEmpty()) {
            LOGGER.error("Post description empty");
            modelAndView.addObject("successMessage", "empty description");
            modelAndView.setViewName("update-post");
            return modelAndView;
        }
        if (updatedPost.getContent().trim().isEmpty()) {
            LOGGER.error("Post content empty");
            modelAndView.addObject("successMessage", "empty content");
            modelAndView.setViewName("update-post");
            return modelAndView;
        }
        if (postRepository.findByTitle(updatedPost.getTitle().trim()) != null &&
                (!updatedPost.getTitle().trim().equals(postRepository.findById(postId).get().getTitle()))) {
            LOGGER.error("Post title already exist");
            modelAndView.addObject("successMessage", "title already exist");
            modelAndView.setViewName("update-post");
            return modelAndView;
        }

        Post post = postRepository.findById(postId).get();
        post.setTitle(updatedPost.getTitle());
        post.setDescription(updatedPost.getDescription());
        post.setContent(updatedPost.getContent());
        if (isPublished.equals("publish")) {
            post.setPublished(true);
        } else {
            post.setPublished(false);
        }
        saveTag(tagString, post);
        LOGGER.debug("post updated successfully {}", post);
        modelAndView.addObject("successMessage", "post updated successfully");
        modelAndView.setViewName("update-post");
        return modelAndView;
    }

    public Post findById(Long id){
        return postRepository.findById(id).get();
    }

    private void saveTag(String tagString,Post post){
        String[] tagArray = tagString.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
        List<Tag> tagList = tagRepository.findAll();
        boolean flag;
        for (int i = 0; i < tagArray.length; i++) {
            flag = true;
            for (Tag oldTag : tagList) {
                if (oldTag.getName().equals(tagArray[i].trim())) {
                    post.getTags().add(oldTag);
                    flag = false;
                    break;
                }
            }
            if (flag) {
                if (!tagArray[i].trim().equals("")) {
                    Tag newTag = new Tag(tagArray[i].trim());
                    post.getTags().add(newTag);
                }
            }
        }
        postRepository.save(post);
    }

    public void deleteById(Long postId){
        postRepository.deleteById(postId);
    }
}

