package com.example.jpa.service;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.event.OnRegistrationCompleteEvent;
import com.example.jpa.model.Author;
import com.example.jpa.model.PasswordResetToken;
import com.example.jpa.model.VerificationToken;
import com.example.jpa.repository.AuthorRepository;
import com.example.jpa.repository.VerificationTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.jpa.util.Constant.EMAIL_PATTERN;

@Service
@Transactional
public class RegistrationService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessageSource messages;

    @Autowired
    IUserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Environment env;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public ModelAndView showSignUpPage() {
        LOGGER.debug("Rendering registration page.");
        final AuthorDto authordto = new AuthorDto();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("authordto", authordto);
        modelAndView.setViewName("sign-up");
        return modelAndView;
    }

    public ModelAndView registerAccount(AuthorDto authorDto, BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {
        LOGGER.debug("Registering user account with information: {}", authorDto);
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            if (authorDto.getName().trim().isEmpty()) {
                LOGGER.error("empty name field");
                modelAndView.addObject("successMessage", "invalid name");
            } else if (!isValidEmail(authorDto.getEmail().trim())) {
                LOGGER.error("invalid email");
                modelAndView.addObject("successMessage", "invalid email");
            } else if (authorDto.getPassword().trim().isEmpty()) {
                LOGGER.error("password field is empty");
                modelAndView.addObject("successMessage", "invalid password");
            } else if (!authorDto.getEmail().trim().equals(authorDto.getMatchingPassword().trim())) {
                LOGGER.error("password mismatch");
                modelAndView.addObject("successMessage", "password mismatch");
            }
            return modelAndView;
        } else {
            Author registered = userService.registerNewUserAccount(authorDto);
            if (registered == null) {
                modelAndView.addObject("successMessage", "user already exists");
                modelMap.addAttribute("bindingResult", bindingResult);
                LOGGER.error("Error occurred in Registration details, user with this email already registered: {}", authorDto);
                return modelAndView;
            } else {
                try {
                    final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
                    eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));
                } catch (final Exception ex) {
                    LOGGER.error("Unable to register user", ex);
                    modelAndView.addObject("successMessage", "connect to internet to send the mail");
                    return modelAndView;
                }
                modelAndView.setViewName("successRegister.html");
                modelAndView.addObject("userEmail", registered.getEmail());
                LOGGER.debug("registered successfully: {}", registered);
                return modelAndView;
            }
        }
    }

    public String confirmRegister(HttpServletRequest request, Model model, String token) {
        LOGGER.debug("rendering registration confirmation");
        final Locale locale = request.getLocale();
        final VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            final String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            LOGGER.error("invalid verificatioin token: {}", token);
            return "redirect:/badUser.html?lang=" + locale.getLanguage();
        }
        final Author author = verificationToken.getAuthor();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));
            model.addAttribute("expired", true);
            model.addAttribute("token", token);
            LOGGER.error("token expired: {}", token);
            return "redirect:/badUser.html?lang=" + locale.getLanguage();
        }

        author.setEnabled(true);
        userService.saveRegisteredUser(author);
        model.addAttribute("message", messages.getMessage("message.accountVerified", null, locale));
        LOGGER.debug("Author verified with token: {}", token);
        return "registrationConfirm";
    }

    public ModelAndView resendVerificationMail(HttpServletRequest request, String userEmail) {
        LOGGER.debug("rendering resend Verification mail");
        Author registered = authorRepository.findByEmail(userEmail);
        ModelAndView modelAndView = new ModelAndView();
        if (registered == null) {
            modelAndView.addObject("successMessage", "user does not exists");
            LOGGER.error("Author does not exist with email : {}", userEmail);
            return modelAndView;
        } else {
            VerificationToken existingToken = verificationTokenRepository.findByAuthor(registered);
            final VerificationToken newToken = userService.generateNewVerificationToken(existingToken.getToken());
            final Author author = userService.getUser(newToken.getToken());
            try {
                final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
                final SimpleMailMessage email = constructResetVerificationTokenEmail(appUrl, request.getLocale(), newToken, author);
                mailSender.send(email);
            } catch (final MailAuthenticationException e) {
                LOGGER.error("MailAuthenticationException", e);
                modelAndView.addObject("successMessage", "Mail Authentication Exception");
                return modelAndView;
            } catch (final Exception e) {
                LOGGER.error(e.getLocalizedMessage(), e);
                modelAndView.addObject("successMessage", "connect to internet to send the mail");
                return modelAndView;
            }
            modelAndView.setViewName("successRegister.html");
            modelAndView.addObject("userEmail", userEmail);
            LOGGER.debug("verification mail resent successfully to mail: {}", userEmail);
            return modelAndView;
        }
    }

    public String resetPassword(Model model, String userEmail, HttpServletRequest request) {
        LOGGER.debug("rendering resetPassword");
        final Author author = userService.findUserByEmail(userEmail);
        if (author == null) {
            model.addAttribute("message", messages.getMessage("message.userNotFound", null, request.getLocale()));
            LOGGER.error("Author does not exist with email: {}", userEmail);
            return "redirect:/forgetPassword.html?lang=" + request.getLocale().getLanguage();
        }
        final String token = UUID.randomUUID().toString();
        userService.createPasswordResetTokenForUser(author, token);
        try {
            final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            final SimpleMailMessage email = constructResetPasswordTokenEmail(appUrl, request.getLocale(), token, author);
            mailSender.send(email);
        } catch (final MailAuthenticationException e) {
            LOGGER.error("MailAuthenticationException", e);
            return "redirect:/emailError.html?lang=" + request.getLocale().getLanguage();
        } catch (final Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            model.addAttribute("message", e.getLocalizedMessage());
            return "redirect:/forgetPassword.html";
        }
        LOGGER.debug("Password reset verification mail sent successfully to mail: {}", userEmail);
        model.addAttribute("message", messages.getMessage("message.resetPasswordEmail", null, request.getLocale()));
        return "redirect:/sign-in.html";
    }

    public String changePassword(Model model, HttpServletRequest request, String token, Long id) {
        LOGGER.debug("rendering changePassword");
        final Locale locale = request.getLocale();
        final PasswordResetToken passToken = userService.getPasswordResetToken(token);
        final Author author = passToken.getAuthor();
        if ((passToken == null) || (author.getId() != id)) {
            final String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            LOGGER.error("invalid token: {}", token);
            return "redirect:/sign-in.html?lang=" + locale.getLanguage();
        }
        final Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));
            LOGGER.error("token expired: {}", token);
            return "redirect:/sign-in.html?lang=" + locale.getLanguage();
        }
        return "redirect:/updatePassword.html?lang=" + locale.getLanguage();
    }

    public String savePassword(Model model, HttpServletRequest request, String password) {
        LOGGER.debug("rendering savePassword");
        final Locale locale = request.getLocale();
        final Author author = (Author) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.changeUserPassword(author, password);
        model.addAttribute("message", messages.getMessage("message.resetPasswordSuc", null, locale));
        return "redirect:/sign-in.html?lang=" + locale;
    }

    public ModelAndView showAuthorSettingPage(Authentication authentication) {
        Author author = (Author) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("author", author);
        modelAndView.setViewName("setting");
        return modelAndView;
    }

    public ModelAndView updateAuthorSetting(ModelMap modelMap, Model model, HttpServletRequest request,
                                            BindingResult bindingResult, Authentication authentication,
                                            AuthorDto authorDto) {
        LOGGER.debug("rendering updateAuthorSetting");
        final Locale locale = request.getLocale();
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            if (authorDto.getName().trim().equals("")) {
                modelAndView.addObject("successMessage", "invalid name");
                LOGGER.error("empty string in name {}", authorDto);
                modelAndView.setViewName("setting");
                modelAndView.addObject("author", authorDto);
                return modelAndView;
            } else if (!isValidEmail(authorDto.getEmail())) {
                modelAndView.addObject("successMessage", "invalid email");
                LOGGER.error("invalid email {}", authorDto);
                modelAndView.setViewName("setting");
                modelAndView.addObject("author", authorDto);
                return modelAndView;
            } else if (authorDto.getPassword().trim().isEmpty()) {
                modelAndView.addObject("successMessage", "empty password");
                LOGGER.error("empty password {}", authorDto);
                modelAndView.setViewName("setting");
                modelAndView.addObject("author", authorDto);
                return modelAndView;
            } else if (!authorDto.getPassword().trim().equals(authorDto.getMatchingPassword().trim())) {
                modelAndView.addObject("successMessage", "password mismatch");
                LOGGER.error("password mismatch {}", authorDto);
                modelAndView.setViewName("setting");
                modelAndView.addObject("author", authorDto);
                return modelAndView;
            } else {
                modelAndView.addObject("successMessage", "invalid password");
                LOGGER.error("password invalid {}", authorDto);
                modelAndView.setViewName("setting");
                modelAndView.addObject("author", authorDto);
                return modelAndView;
            }
        }
        Author registered = (Author) authentication.getPrincipal();
        if (!registered.getEmail().equals(authorDto.getEmail())) {
            registered.setEmail(authorDto.getEmail());
            registered.setEnabled(false);
        }
        registered.setName(authorDto.getName());
        registered.setPassword(passwordEncoder.encode(authorDto.getPassword()));
        authorRepository.save(registered);
        VerificationToken existingToken = verificationTokenRepository.findByAuthor(registered);
        final VerificationToken newToken = userService.generateNewVerificationToken(existingToken.getToken());
        final Author author = userService.getUser(newToken.getToken());
        try {
            final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            final SimpleMailMessage email = constructResetVerificationTokenEmail(appUrl, request.getLocale(), newToken, author);
            mailSender.send(email);
        } catch (final MailAuthenticationException mailAuthenticationException) {
            LOGGER.error("MailAuthenticationException", mailAuthenticationException);
        } catch (final Exception exception) {
            LOGGER.error(exception.getLocalizedMessage(), exception);
            model.addAttribute("message", exception.getLocalizedMessage());
        }
        modelAndView.addObject("successMessage", "verification link sent to your mail");
        LOGGER.debug("Updated Author settings successfully");
        modelAndView.addObject("author", authorDto);
        return modelAndView;
    }

    private final SimpleMailMessage constructResetVerificationTokenEmail(final String contextPath, final Locale locale, final VerificationToken newToken, final Author author) {
        LOGGER.debug("rendering constructResetVerificationTokenEmail");
        final String confirmationUrl = contextPath + "/registrationConfirm.html?token=" + newToken.getToken();
        final String message =
                messages.getMessage("message.resendToken", null, locale);
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject("Resend Registration Token");
        email.setText(message + " \r\n" + confirmationUrl);
        email.setTo(author.getEmail());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private final SimpleMailMessage constructResetPasswordTokenEmail(final String contextPath, final Locale locale, final String token, final Author author) {
        LOGGER.debug("rendering constructResetPasswordTokenEmail");
        final String url = contextPath + "/author/changePassword?id=" + author.getId() + "&token=" + token;
        final String message = messages.getMessage("message.resetPassword", null, locale);
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(author.getEmail());
        email.setSubject("Reset Password");
        email.setText(message + " \r\n" + url);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private boolean isValidEmail(final String email) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
