package com.example.jpa.service;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.exception.UserAlreadyExistException;
import com.example.jpa.model.Author;
import com.example.jpa.model.PasswordResetToken;
import com.example.jpa.repository.AuthorRepository;
import com.example.jpa.repository.PasswordResetTokenRepository;
import com.example.jpa.repository.RoleRepository;
import com.example.jpa.repository.VerificationTokenRepository;
import com.example.jpa.model.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.transaction.Transactional;
import java.awt.print.Pageable;
import java.util.*;

@Service
@Transactional
public class AuthorService implements IUserService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    // API

    @Override
    public Page<Author> findAll(int page){
        return  authorRepository.findAll(PageRequest.of(page, 4));
    }

    public void makeAdmin(Model model, Long authorId,int page){
        Author author = authorRepository.getOne(authorId);
        author.getRoles().remove(roleRepository.findByName("ROLE_USER"));
        author.getRoles().add(roleRepository.findByName("ROLE_ADMIN"));
        authorRepository.save(author);
        model.addAttribute("users",findAll(page));
    }

    @Override
    public Author registerNewUserAccount(final AuthorDto accountDto) {
        if (emailExists(accountDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email adress: " + accountDto.getEmail());
        }
        final Author author = new Author();

        author.setName(accountDto.getName());
        author.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        author.setEmail(accountDto.getEmail());
        author.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
        return authorRepository.save(author);
    }

    @Override
    public Author getUser(final String verificationToken) {
        final VerificationToken token = tokenRepository.findByToken(verificationToken);
        if (token != null) {
            return token.getAuthor();
        }
        return null;
    }

    @Override
    public VerificationToken getVerificationToken(final String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    @Override
    public void saveRegisteredUser(final Author author) {
        authorRepository.save(author);
    }

    @Override
    public void createVerificationTokenForUser(final Author author, final String token) {
        final VerificationToken myToken = new VerificationToken(token, author);
        tokenRepository.save(myToken);
    }

    @Override
    public VerificationToken generateNewVerificationToken(final String existingVerificationToken) {
        VerificationToken vToken = tokenRepository.findByToken(existingVerificationToken);
        vToken.updateToken(UUID.randomUUID()
            .toString());
        vToken = tokenRepository.save(vToken);
        return vToken;
    }

    @Override
    public void createPasswordResetTokenForUser(final Author author, final String token) {
        final PasswordResetToken myToken = new PasswordResetToken(token, author);
        passwordTokenRepository.save(myToken);
    }

    @Override
    public Author findUserByEmail(final String email) {
        return authorRepository.findByEmail(email);
    }

    @Override
    public PasswordResetToken getPasswordResetToken(final String token) {
        return passwordTokenRepository.findByToken(token);
    }

    @Override
    public void changeUserPassword(final Author author, final String password) {
        author.setPassword(passwordEncoder.encode(password));
        authorRepository.save(author);
    }

    private boolean emailExists(final String email) {
        return authorRepository.findByEmail(email) != null;
    }

}
