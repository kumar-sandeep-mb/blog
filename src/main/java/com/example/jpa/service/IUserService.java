package com.example.jpa.service;

import com.example.jpa.exception.UserAlreadyExistException;
import com.example.jpa.model.Author;
import com.example.jpa.model.PasswordResetToken;
import com.example.jpa.model.VerificationToken;
import com.example.jpa.dto.AuthorDto;
import org.springframework.data.domain.Page;

public interface IUserService {

    Page<Author> findAll(int page);

    Author registerNewUserAccount(AuthorDto accountDto) throws UserAlreadyExistException;

    Author getUser(String verificationToken);

    void saveRegisteredUser(Author author);

    void createVerificationTokenForUser(Author author, String token);

    VerificationToken getVerificationToken(String VerificationToken);

    VerificationToken generateNewVerificationToken(String token);

    void createPasswordResetTokenForUser(Author author, String token);

    Author findUserByEmail(String email);

    PasswordResetToken getPasswordResetToken(String token);

    void changeUserPassword(Author author, String password);
}
