package com.example.jpa.service;

import com.example.jpa.model.Author;
import com.example.jpa.model.Comment;
import com.example.jpa.repository.CommentRepository;
import com.example.jpa.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CommentService {
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    PostRepository postRepository;

    public void saveComment(Comment comment, Authentication authentication,Long postId){
        comment.setAuthor((Author)authentication.getPrincipal());
        postRepository.findById(postId).map(post -> {
            comment.setPost(post);
            commentRepository.save(comment);
            return "redirect:/post/"+postId+"";
        });
    }
    public void updateComment(String commentMessage,Long id){
        commentRepository.findById(id).map(comment -> {
            comment.setCommentMessage(commentMessage);
            commentRepository.save(comment);
            return "redirect:/";
        });
    }
    public void deleteComment(Long id){
        commentRepository.delete(commentRepository.findById(id).get());
    }

    public Page<Comment> findByPostId(Long postId, Pageable pageable){
        return commentRepository.findByPostId(postId,pageable);
    }

    public Comment findById(Long id){
        return commentRepository.findById(id).get();
    }
}
