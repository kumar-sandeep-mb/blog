package com.example.jpa.controller;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.model.Author;
import com.example.jpa.model.VerificationToken;
import com.example.jpa.repository.VerificationTokenRepository;
import com.example.jpa.service.IUserService;
import com.example.jpa.service.RegistrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class AuthorController {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    IUserService userService;

    @GetMapping("/setting")
    public ModelAndView AuthorSetting(Authentication authentication){
        return registrationService.showAuthorSettingPage(authentication);
    }

    @PostMapping("/setting")
    public ModelAndView updateAuthorSetting(@ModelAttribute("userdto") @Valid final AuthorDto authorDto,
                                          BindingResult bindingResult, ModelMap modelMap,Authentication authentication,
                                          final HttpServletRequest request,Model model){
        return registrationService.updateAuthorSetting(modelMap, model, request, bindingResult, authentication, authorDto);
    }
}
