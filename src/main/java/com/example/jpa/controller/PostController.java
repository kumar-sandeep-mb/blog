package com.example.jpa.controller;

import com.example.jpa.model.Author;
import com.example.jpa.model.Post;
import com.example.jpa.service.CommentService;
import com.example.jpa.service.PostService;
import com.example.jpa.repository.PostRepository;
import com.example.jpa.repository.TagRepository;
import com.example.jpa.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PostController {
    @Autowired
    private TagService tagService;
    @Autowired
    private PostService postService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private TagRepository tagRepository;

    @GetMapping("/")
    public String showPost(Model model, @RequestParam(defaultValue = "0") int page){
        model.addAttribute("posts", postService.getAllPost(page));
        model.addAttribute("tags", tagService.getAllTag());
        model.addAttribute("current",page);
        model.addAttribute("targetUrl","post");
        return "index";
    }

    @GetMapping("/profile")
    public String showPost(Model model, @RequestParam(defaultValue = "0") int page, Authentication authentication){
        model.addAttribute("posts", postService.getAuthorPost(page,(Author)authentication.getPrincipal()));
        model.addAttribute("current", page);
        return "profile";
    }

    @GetMapping("/userFeed")
    public String showUserPost(Model model, @RequestParam(defaultValue = "0") int page, Authentication authentication){
        model.addAttribute("posts", postService.getAuthorPost(page,(Author)authentication.getPrincipal()));
        model.addAttribute("tags", tagService.getAllTag());
        model.addAttribute("current",page);
        model.addAttribute("targetUrl","post");
        return "index";
    }

    @GetMapping("/drafts")
    public String showDrafts(Model model, @RequestParam(defaultValue = "0") int page, Authentication authentication){
        model.addAttribute("posts", postService.getAuthorDraft(page,(Author)authentication.getPrincipal()));
        model.addAttribute("tags", tagService.getAllTag());
        model.addAttribute("current",page);
        model.addAttribute("targetUrl","drafts");
        return "index";
    }

    @GetMapping("/search-post")
    public String showPostSearchedByString(Model model,@RequestParam String searchString, @RequestParam(defaultValue = "0") int page){
        model.addAttribute("posts", postService.getBySearch(page,searchString));
        model.addAttribute("tags", tagService.getAllTag());
        model.addAttribute("current",page);
        return "index";
    }

    @GetMapping("/new-post")
    public ModelAndView newPost(){
        return postService.showNewPostPage();
    }

    @PostMapping("/new-post")
    public ModelAndView savePost(ModelMap modelMap,Post post, @RequestParam("tagName") String tagString, @RequestParam("isPublished") String isPublished, Authentication authentication) {
        return postService.save(modelMap,post,authentication,isPublished,tagString);
    }

    @GetMapping("/post/{postId}")
    public String findPost(Model model, @PathVariable("postId") Long postId,Pageable pageable){
        model.addAttribute("post",postService.findById(postId));
        model.addAttribute("comments", commentService.findByPostId(postId, pageable));
        return "article";
    }

    @GetMapping("/drafts/{postId}")
    public String findDraft(Model model, @PathVariable("postId") Long postId,Pageable pageable) {
        model.addAttribute("post", postService.findById(postId));
        return "update-post";
    }

    @GetMapping("/filter-post/{tagId}")
    public String showFilteredPost(Model model,@PathVariable Long tagId, @RequestParam(defaultValue = "0") int page){
//        Page<Post> pages =  tagRepository.findById(tagId).get().getPosts();
        model.addAttribute("posts", postRepository.findAll(PageRequest.of(page, 4,Sort.by("updatedAt").descending())));
        model.addAttribute("tags", tagRepository.findAll(PageRequest.of(page,5)));
        model.addAttribute("current",page);
        return "index";
    }

    @GetMapping("/post/{postId}/update-post")
    public ModelAndView showUpdatePostPage(@PathVariable Long postId) {
        return postService.showUpdatePostPage(postId);
    }

    @PostMapping("post/{postId}/update-post")
    public ModelAndView updatePost(@PathVariable Long postId, Post updatedPost,@RequestParam("tagName") String tagString,@RequestParam("isPublished") String isPublished) {
        return postService.update(postId, updatedPost, isPublished, tagString);
    }


    @GetMapping("post/{postId}/delete-post")
    public String deleteCountry(@PathVariable Long postId){
        postService.deleteById(postId);
        return "redirect:/";
    }
}
