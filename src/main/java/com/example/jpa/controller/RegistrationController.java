package com.example.jpa.controller;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.repository.VerificationTokenRepository;
import com.example.jpa.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class RegistrationController {

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private RegistrationService registrationService;

    public RegistrationController() {
        super();
    }

    @GetMapping("/sign-up")
    public ModelAndView showRegistrationForm() {
        return registrationService.showSignUpPage();
    }

    @PostMapping("/sign-up")
    public ModelAndView registerUserAccount(@ModelAttribute("authordto") @Valid final AuthorDto authorDto,
                                            BindingResult bindingResult, ModelMap modelMap,
                                            final HttpServletRequest request) {
        return registrationService.registerAccount(authorDto, bindingResult, modelMap, request);
    }

    @GetMapping("/registrationConfirm")
    public String confirmRegistration(final HttpServletRequest request, final Model model, @RequestParam("token") final String token) {
        return registrationService.confirmRegister(request, model, token);
    }

    @PostMapping("/resendVerificationMail")
    public ModelAndView resendMail(final Model model, final HttpServletRequest request, @RequestParam(name = "userEmail") String userEmail) {
        return registrationService.resendVerificationMail(request, userEmail);
    }

    @PostMapping("/author/resetPassword")
    public String resetPassword(final HttpServletRequest request, final Model model, @RequestParam("email") final String userEmail) {
        return registrationService.resetPassword(model, userEmail, request);
    }

    @GetMapping("/author/changePassword")
    public String changePassword(final HttpServletRequest request, final Model model, @RequestParam("id") final long id, @RequestParam("token") final String token) {
        return registrationService.changePassword(model, request, token, id);
    }

    @PostMapping("/author/savePassword")
    @PreAuthorize("hasRole('READ_PRIVILEGE')")
    public String savePassword(final HttpServletRequest request, final Model model, @RequestParam("password") final String password) {
        return registrationService.savePassword(model, request, password);
    }
}
