package com.example.jpa.controller;

import com.example.jpa.repository.RoleRepository;
import com.example.jpa.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {
    @Autowired
    AuthorService authorService;
    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/users")
    public String showUsers(Model model,@RequestParam(defaultValue = "0") int page){
        model.addAttribute("users",authorService.findAll(page));
        model.addAttribute("current",page);
        return "users";
    }

    @GetMapping("makeAdmin/{authorId}")
    public String makeAdmin(Model model, @PathVariable Long authorId, @RequestParam(defaultValue = "0") int page){
        authorService.makeAdmin(model, authorId, page);
        model.addAttribute("current",page);
        return "users";
    }
}
