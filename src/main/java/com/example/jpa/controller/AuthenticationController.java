package com.example.jpa.controller;

import com.example.jpa.model.Author;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthenticationController {

    @GetMapping("/sign-in")
    public ModelAndView showSignInPage() {
        ModelAndView modelAndView = new ModelAndView();
        Author author = new Author();
        modelAndView.addObject("user", author);
        modelAndView.setViewName("sign-in");
        return modelAndView;
    }
}