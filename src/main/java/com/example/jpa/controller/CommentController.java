package com.example.jpa.controller;

import com.example.jpa.model.Comment;
import com.example.jpa.service.CommentService;
import com.example.jpa.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private PostService postService;

    @PostMapping("/post/{postId}/save-comment")
    public String createComment(@PathVariable(value = "postId") Long postId, Comment comment, Authentication authentication) {
        commentService.saveComment(comment,authentication,postId);
        return "redirect:/post/"+postId+"";
    }

    @GetMapping("post/{postId}/comment/{commentId}")
    public String showUpdateCommentPage(Model model, @PathVariable("commentId") Long commentId,@PathVariable Long postId) {
        model.addAttribute("comment", commentService.findById(commentId));
        model.addAttribute("post",postService.findById(postId));
        return "update-comment";
    }

    @PostMapping("post/{postId}/comment/{commentId}/update-comment")
    public String updateComment(@PathVariable Long commentId,@PathVariable Long postId, Comment updatedComment) {
        commentService.updateComment(updatedComment.getCommentMessage(),commentId);
        return "redirect:/post/"+postId+"";
    }

    @GetMapping("post/{postId}/comment/{commentId}/delete-comment")
    public String deleteComment(@PathVariable Long commentId,@PathVariable Long postId) {
        commentService.deleteComment(commentId);
        return "redirect:/post/"+postId+"";
    }
}
